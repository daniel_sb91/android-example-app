package com.example.app.com.example.app.networking;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.app.R;
import com.example.app.com.example.app.objects.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Web Service that gets an amount of places near to a specific place (Is using the Foursquare API)
 */
public class GetPlacesService {

    private final String endPoint = "https://api.foursquare.com/v2/venues/search?near=@placeToSearch@" +
            "&oauth_token=GFMYRFCNDRGCKMGVHC4FQBUZIVLYJZGMY3S5CHTXGXCJJFKJ&v=20140805&limit=10";
    private GetPlacesArrayListener mListener;
    private Context mContext;
    private String mServiceUrl;

    public GetPlacesService(Context context, String placeToSearch, GetPlacesArrayListener listener){
        mListener = listener;
        mContext = context;
        mServiceUrl = endPoint.replace("@placeToSearch@", placeToSearch);
    }

    /**
     * GET request to Foursquare API
     */
    public void getNearPlaces(){
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, mServiceUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response){
                        buildPlacesArray(response);
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        mListener.onFailure(error);
                    }
        });

        VolleySingleton.getInstance(mContext).getRequestQueue().add(jsObjRequest);
    }

    /**
     * Builds a new array of objects with the places returned by the request
     * @param response
     */
    private void buildPlacesArray(JSONObject response){
        try {
            JSONObject responseObjectData = response.getJSONObject("response");
            JSONArray venuesArrayData = responseObjectData.getJSONArray("venues");
            ArrayList<Place> placesArray = new ArrayList<Place>();

            for(int dataElementCounter = 0; dataElementCounter < venuesArrayData.length(); dataElementCounter++){
                JSONObject venueObject = venuesArrayData.getJSONObject(dataElementCounter);
                Place place = new Place();
                place.setName(venueObject.getString("name"));
                place.setLatitude(venueObject.getJSONObject("location").getDouble("lat"));
                place.setLongitude(venueObject.getJSONObject("location").getDouble("lng"));
                placesArray.add(place);
            }

            mListener.onSuccess(placesArray);

        } catch (JSONException e) {
            e.printStackTrace();
            mListener.onParsingObjectFailure(mContext.getString(R.string.generic_error));
        }
    }
}
