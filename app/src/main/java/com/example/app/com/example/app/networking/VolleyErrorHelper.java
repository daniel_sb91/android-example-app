package com.example.app.com.example.app.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.example.app.R;


import android.content.Context;

/**
 * Returns an appropriate error message when an error occurs.
 */
public  class VolleyErrorHelper {
	
	public static String getMessage(Object error, Context context){
		if(error instanceof TimeoutError){
			return context.getResources().getString(R.string.time_out_error);
		}
		else if (isServerProblem(error)){
			return context.getResources().getString(R.string.server_error);
		}
		else if (isNetworkProblem(error)){
			return context.getResources().getString(R.string.network_error);
		}
		else
			return context.getResources().getString(R.string.generic_error);
	}
	
	/**
	 * Determines whether the error is related to network.
	 * @param error
	 * @return
	 */
	private static boolean isNetworkProblem(Object error){
		return (error instanceof NetworkError) || (error instanceof NoConnectionError);
	}
	
	/**
	 * Determines whether the error is related to the server. 
	 * @param error
	 * @return
	 */
	private static boolean isServerProblem(Object error){
		return (error instanceof ServerError) || (error instanceof AuthFailureError);
	}

}
