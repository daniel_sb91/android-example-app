package com.example.app.com.example.app.activities;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.app.R;
import com.example.app.com.example.app.fragments.MainScreenFragment;

public class MainScreenActivity extends ActionBarActivity {

    public static final String PLACE_TO_SEARCH_KEY = "place to search";
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setMainScreenFragment();
    }

    @Override
    public void onBackPressed() {
        showLogoutDialog();
    }

    private void showLogoutDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.logout_dialog_title);
        dialogBuilder.setMessage(R.string.logout_dialog_message);
        dialogBuilder.setNegativeButton(R.string.logout_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialogBuilder.setPositiveButton(R.string.logout_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        dialogBuilder.setCancelable(true);
        dialogBuilder.create().show();
    }

    private void setMainScreenFragment(){
        FragmentManager fragmentManager = getFragmentManager();
        MainScreenFragment mainScreenFragment = MainScreenFragment.newInstance();
        fragmentManager.beginTransaction().
                replace(R.id.fragment_container, mainScreenFragment, "mainScreenFragment")
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actions_about_me) {
            showAboutMeDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showAboutMeDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.overflow_dialog_title);
        dialogBuilder.setMessage(R.string.overflow_dialog_message);
        dialogBuilder.setNegativeButton(R.string.overflow_dialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setCancelable(true);
        dialogBuilder.create().show();
    }

}
