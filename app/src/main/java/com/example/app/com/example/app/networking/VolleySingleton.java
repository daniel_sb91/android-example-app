package com.example.app.com.example.app.networking;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Volley is a library that handles networking for android
 * in a easier and faster way.
 * Singleton class that handles the volley request queue. 
 * @author Daniel Sanchez <daniel.sanchez@avantica.net>
 *
 */
public class VolleySingleton{
	
	private static VolleySingleton instance;
    private RequestQueue requestQueue;
 
    private VolleySingleton(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }
 
    public static VolleySingleton getInstance(Context context) {
        if (instance == null) {
            instance = new VolleySingleton(context);
        }
        return instance;
    }
 
    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

}
