package com.example.app.com.example.app.networking;

import com.android.volley.VolleyError;
import com.example.app.com.example.app.objects.Place;

import java.util.ArrayList;

/**
 * Created by daniel.sanchez on 5/12/2015.
 */
public interface GetPlacesArrayListener {

    public void onSuccess(ArrayList<Place> objects);
    public void onFailure(VolleyError error);
    public void onParsingObjectFailure(String errorMessage);
}
