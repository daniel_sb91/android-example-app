package com.example.app.com.example.app.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.app.R;
import com.example.app.com.example.app.adapters.RecyclerViewAdapter;
import com.example.app.com.example.app.networking.GetPlacesArrayListener;
import com.example.app.com.example.app.networking.GetPlacesService;
import com.example.app.com.example.app.networking.VolleyErrorHelper;
import com.example.app.com.example.app.objects.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapScreenActivity extends ActionBarActivity implements OnMapReadyCallback {

    private final double LATITUDE_COSTA_RICA = 9.83;
    private final double LONGITUDE_COSTA_RICA = -83.9;
    private final float ZOOM_LEVEL = 7.0f;
    private Toolbar mToolbar;
    private String mSelectedPlace;
    private Location mMyCurrentLocation;
    private Location mSelectedPlaceLocation;
    private TextView mDistanceTextView;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map_screen);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDistanceTextView = (TextView) findViewById(R.id.text_view_distance);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setupMapFragment();
        setupSelectedPlaceLocation();
    }

    private void setupMapFragment(){
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setupSelectedPlaceLocation(){
        mSelectedPlace = getIntent().getStringExtra(RecyclerViewAdapter.SELECTED_PLACE_NAME);
        double selectedPlaceLatitude = getIntent().getDoubleExtra(RecyclerViewAdapter.LATITUDE_KEY, 0);
        double selectedPlaceLongitude = getIntent().getDoubleExtra(RecyclerViewAdapter.LONGITUDE_KEY, 0);
        mSelectedPlaceLocation = new Location("");
        mSelectedPlaceLocation.setLatitude(selectedPlaceLatitude);
        mSelectedPlaceLocation.setLongitude(selectedPlaceLongitude);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about_me_map_screen) {
            showAboutMeDialog();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showAboutMeDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.overflow_dialog_title);
        dialogBuilder.setMessage(R.string.overflow_dialog_message);
        dialogBuilder.setNegativeButton(R.string.overflow_dialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setCancelable(true);
        dialogBuilder.create().show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(mMap.MAP_TYPE_NORMAL);
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(
                new LatLng(LATITUDE_COSTA_RICA, LONGITUDE_COSTA_RICA), ZOOM_LEVEL));

        if(mSelectedPlaceLocation.getLatitude() != 0 &&
           mSelectedPlaceLocation.getLongitude() != 0)
            paintSelectedPlaceMarker(googleMap);
    }

    private void paintSelectedPlaceMarker(GoogleMap googleMap){
        googleMap.addMarker(new MarkerOptions().
                position(new LatLng(mSelectedPlaceLocation.getLatitude(),
                        mSelectedPlaceLocation.getLongitude())).title(mSelectedPlace));
    }

    /**
     * Calculate distance between two locations
     * @param view
     */
    public void calculateDistance(View view){
        mMyCurrentLocation = mMap.getMyLocation();
        float distanceBetweenLocations;
        if(mMyCurrentLocation != null) {
            distanceBetweenLocations = mMyCurrentLocation.distanceTo(mSelectedPlaceLocation);
            mDistanceTextView.setText(getString(R.string.distance) + " " +distanceBetweenLocations + " meters");
        } else
            showCurrentLocationIsNotAvailable();
    }

    private void showCurrentLocationIsNotAvailable(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.no_distance_available_dialog_title);
        dialogBuilder.setMessage(R.string.no_distance_available_dialog_message);
        dialogBuilder.setNegativeButton(R.string.no_distance_available_dialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setCancelable(true);
        dialogBuilder.create().show();
    }
}
