package com.example.app.com.example.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.example.app.R;
import com.example.app.com.example.app.activities.PlacesListActivity;
import com.example.app.com.example.app.networking.GetPlacesArrayListener;
import com.example.app.com.example.app.networking.GetPlacesService;
import com.example.app.com.example.app.networking.VolleyErrorHelper;
import com.example.app.com.example.app.objects.Place;

import java.util.ArrayList;

public class MainScreenFragment extends Fragment {

    public static final String PLACE_TO_SEARCH = "places list key";
    private EditText mSearchPlacesEditText;
    private Button mSearchPlacesButton;

    public static MainScreenFragment newInstance() {
        MainScreenFragment fragment = new MainScreenFragment();
        return fragment;
    }

    public MainScreenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main_screen, container, false);
        mSearchPlacesEditText = (EditText) rootView.findViewById(R.id.edit_text_search_places);
        mSearchPlacesButton = (Button) rootView.findViewById(R.id.button_search);
        mSearchPlacesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPlace();
            }
        });

        return rootView;
    }

    public void searchPlace(){
        String placeToSearch = mSearchPlacesEditText.getText().toString();
        callPlacesListActivity(placeToSearch);
    }

    private void callPlacesListActivity(String placeToSearch){
        Intent intent = new Intent(getActivity(), PlacesListActivity.class);
        intent.putExtra(PLACE_TO_SEARCH, placeToSearch);
        startActivity(intent);
    }

}
