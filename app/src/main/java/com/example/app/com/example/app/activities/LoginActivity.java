package com.example.app.com.example.app.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;

import com.example.app.R;

public class LoginActivity extends ActionBarActivity {

    private static final String USERNAME = "maka";
    private static final String PASSWORD = "5555";
    private EditText mUsernameEditText;
    private EditText mPasswordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsernameEditText = (EditText) findViewById(R.id.edit_text_username);
        mPasswordEditText = (EditText) findViewById(R.id.edit_text_password);
    }

    /**
     * Login button action
     * @param view
     */
    public void login(View view){

        String username = mUsernameEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();

        if(areValidCredentials(username, password))
            callMainScreenActivity();
        else
            showErrorDialog();
    }

    private boolean areValidCredentials(String username, String password){
        if(username.equals(USERNAME) && password.equals(PASSWORD))
            return true;
        else
            return false;
    }

    private void callMainScreenActivity(){
        Intent intent = new Intent(getApplicationContext(), MainScreenActivity.class);
        startActivity(intent);
    }

    private void showErrorDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.invalid_credentials_dialog_title);
        dialogBuilder.setMessage(R.string.invalid_credentials_dialog_message);
        dialogBuilder.setNegativeButton(R.string.invalid_credentials_dialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setCancelable(true);
        dialogBuilder.create().show();
    }

}
