package com.example.app.com.example.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.com.example.app.activities.MapScreenActivity;
import com.example.app.com.example.app.objects.Place;

import java.util.ArrayList;

/**
 * Created by daniel.sanchez on 5/12/2015.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public static final String LATITUDE_KEY = "latitude key";
    public static final String LONGITUDE_KEY = "longitude key";
    public static final String SELECTED_PLACE_NAME = "selected place name";
    ArrayList<Place> mPlacesList;
    Context mContext;

    /**
     * ViewHolder pattern class
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this
        public TextView mTextViewPlaceName;
        public TextView mTextViewLatitude;
        public TextView mTextViewLongitude;

        public ViewHolder(View view) {
            super(view);
            mTextViewPlaceName = (TextView) view.findViewById(R.id.text_view_place_name);
            mTextViewLatitude = (TextView) view.findViewById(R.id.text_view_latitude);
            mTextViewLongitude = (TextView) view.findViewById(R.id.text_view_longitude);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), MapScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SELECTED_PLACE_NAME, mTextViewPlaceName.getText().toString());
                    intent.putExtra(LATITUDE_KEY, Double.valueOf(mTextViewLatitude.getText().toString()));
                    intent.putExtra(LONGITUDE_KEY, Double.valueOf(mTextViewLongitude.getText().toString()));
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    public RecyclerViewAdapter(Context context, ArrayList<Place> placesList){
        mPlacesList = placesList;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextViewPlaceName.setText(mPlacesList.get(position).getName());
        holder.mTextViewLatitude.setText(String.valueOf(mPlacesList.get(position).getLatitude()));
        holder.mTextViewLongitude.setText(String.valueOf(mPlacesList.get(position).getLongitude()));
    }

    @Override
    public int getItemCount() {
        if(mPlacesList != null)
            return mPlacesList.size();
        else
            return 0;
    }

    /**
     * Updates the data recycler view
     * @param mPlacesList
     */
    public void setmPlacesList(ArrayList<Place> mPlacesList) {
        this.mPlacesList = mPlacesList;
        notifyDataSetChanged();
    }
}
