package com.example.app.com.example.app.objects;

/**
 * Created by daniel.sanchez on 5/12/2015.
 * Place Object
 */
public class Place {

    private String name;
    private double latitude;
    private double longitude;


    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
