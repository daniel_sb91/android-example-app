package com.example.app.com.example.app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.VolleyError;
import com.example.app.R;
import com.example.app.com.example.app.adapters.RecyclerViewAdapter;
import com.example.app.com.example.app.fragments.MainScreenFragment;
import com.example.app.com.example.app.networking.GetPlacesArrayListener;
import com.example.app.com.example.app.networking.GetPlacesService;
import com.example.app.com.example.app.networking.VolleyErrorHelper;
import com.example.app.com.example.app.objects.Place;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlacesListActivity extends ActionBarActivity {

    private Toolbar mToolbar;
    private RecyclerView mRecyclerViewPlaces;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private GetPlacesService mGetPlacesService;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_places_list);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.progress_dialog_message));
        setupRecyclerView();
        String placeToSearch = getIntent().getStringExtra(MainScreenFragment.PLACE_TO_SEARCH);
        getNearPlaces(placeToSearch);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_places_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Initialize the Recycler View
     */
    private void setupRecyclerView(){
        mRecyclerViewPlaces = (RecyclerView) findViewById(R.id.recycler_view_places);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewPlaces.setLayoutManager(mLayoutManager);
        mRecyclerViewAdapter = new RecyclerViewAdapter(getApplicationContext(), null);
        mRecyclerViewPlaces.setAdapter(mRecyclerViewAdapter);
    }


    private void getNearPlaces(String placeToSearch){
        mProgressDialog.show();
        mGetPlacesService = new GetPlacesService(getApplicationContext(), placeToSearch, new GetPlacesArrayListener() {
            @Override
            public void onSuccess(ArrayList<Place> placesList) {
                mProgressDialog.dismiss();
                if(placesList.size() > 0)
                    mRecyclerViewAdapter.setmPlacesList(placesList);
                else
                    showErrorDialog(getString(R.string.no_results_found));
            }

            @Override
            public void onFailure(VolleyError error) {
                mProgressDialog.dismiss();
                Log.i("Bad Server Response", VolleyErrorHelper.getMessage(error, getApplicationContext()));
                showErrorDialog(getString(R.string.no_results_found));
            }

            @Override
            public void onParsingObjectFailure(String errorMessage) {
                mProgressDialog.dismiss();
                showErrorDialog(errorMessage);
            }
        });
        mGetPlacesService.getNearPlaces();
    }

    private void showErrorDialog(String errorMessage){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.message_title);
        dialogBuilder.setMessage(errorMessage);
        dialogBuilder.setNegativeButton(R.string.overflow_dialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        });
        dialogBuilder.setCancelable(true);
        dialogBuilder.create().show();
    }
}
